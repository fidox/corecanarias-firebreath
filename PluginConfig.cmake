#/**********************************************************\ 
#
# Auto-Generated Plugin Configuration file
# for corecanarias browser extension
#
#\**********************************************************/

set(PLUGIN_NAME "corecanariasjs")
set(PLUGIN_PREFIX "corec")
set(COMPANY_NAME "corecanarias")

# ActiveX constants:
set(FBTYPELIB_NAME corecanariasjsLib)
set(FBTYPELIB_DESC "corecanariasjs 1.0 Type Library")
set(IFBControl_DESC "corecanariasjs Control Interface")
set(FBControl_DESC "corecanariasjs Control Class")
set(IFBComJavascriptObject_DESC "corecanariasjs IComJavascriptObject Interface")
set(FBComJavascriptObject_DESC "corecanariasjs ComJavascriptObject Class")
set(IFBComEventSource_DESC "corecanariasjs IFBComEventSource Interface")
set(AXVERSION_NUM "1")

# NOTE: THESE GUIDS *MUST* BE UNIQUE TO YOUR PLUGIN/ACTIVEX CONTROL!  YES, ALL OF THEM!
set(FBTYPELIB_GUID 07b3e91b-8462-5479-bdf4-e000c1d3871c)
set(IFBControl_GUID 6f212c7b-fc68-5f21-a615-39eb0532b4ea)
set(FBControl_GUID 0b046640-0a96-539d-ae26-df88380d40a6)
set(IFBComJavascriptObject_GUID e1e7a0eb-0ff4-5421-b57e-090e63772f1f)
set(FBComJavascriptObject_GUID 32551d7e-80cd-5df4-a956-0e56d0033a48)
set(IFBComEventSource_GUID d890de82-3d52-5004-a1ce-10dc58dcf5c3)
if ( FB_PLATFORM_ARCH_32 )
    set(FBControl_WixUpgradeCode_GUID 6ff7aeb2-1cb4-5940-888c-9b770bac29d0)
else ( FB_PLATFORM_ARCH_32 )
    set(FBControl_WixUpgradeCode_GUID ce6c1fcb-bffd-519f-8f20-f22698e6064e)
endif ( FB_PLATFORM_ARCH_32 )

# these are the pieces that are relevant to using it from Javascript
set(ACTIVEX_PROGID "corecanarias.corecanariasjs")
set(MOZILLA_PLUGINID "corecanarias.com/corecanariasjs")

# strings
set(FBSTRING_CompanyName "corecanarias")
set(FBSTRING_PluginDescription "Privileged javascript functions")
set(FBSTRING_PLUGIN_VERSION "1.0.0.0")
set(FBSTRING_LegalCopyright "Copyright 2013 corecanarias")
set(FBSTRING_PluginFileName "np${PLUGIN_NAME}.dll")
set(FBSTRING_ProductName "corecanarias browser extension")
set(FBSTRING_FileExtents "")
if ( FB_PLATFORM_ARCH_32 )
    set(FBSTRING_PluginName "corecanarias browser extension")  # No 32bit postfix to maintain backward compatability.
else ( FB_PLATFORM_ARCH_32 )
    set(FBSTRING_PluginName "corecanarias browser extension_${FB_PLATFORM_ARCH_NAME}")
endif ( FB_PLATFORM_ARCH_32 )
set(FBSTRING_MIMEType "application/x-corecanariasjs")

# Uncomment this next line if you're not planning on your plugin doing
# any drawing:

set (FB_GUI_DISABLED 1)

# Mac plugin settings. If your plugin does not draw, set these all to 0
set(FBMAC_USE_QUICKDRAW 0)
set(FBMAC_USE_CARBON 0)
set(FBMAC_USE_COCOA 0)
set(FBMAC_USE_COREGRAPHICS 0)
set(FBMAC_USE_COREANIMATION 0)
set(FBMAC_USE_INVALIDATINGCOREANIMATION 0)

# If you want to register per-machine on Windows, uncomment this line
#set (FB_ATLREG_MACHINEWIDE 1)

set (BOOST_FILESYSTEM_V3 1)
add_boost_library(filesystem)