/**********************************************************\

  Auto-generated corecanariasjsAPI.cpp

\**********************************************************/

#include "JSObject.h"
#include "variant_list.h"
#include "DOM/Document.h"
#include "global/config.h"
#include <boost/filesystem.hpp>

#include "corecanariasjsAPI.h"

///////////////////////////////////////////////////////////////////////////////
/// @fn FB::variant corecanariasjsAPI::echo(const FB::variant& msg)
///
/// @brief  Echos whatever is passed from Javascript.
///         Go ahead and change it. See what happens!
///////////////////////////////////////////////////////////////////////////////
FB::variant corecanariasjsAPI::echo(const FB::variant& msg)
{
    static int n(0);
    fire_echo("So far, you clicked this many times: ", n++);

    // return "foobar";
    return msg;
}

///////////////////////////////////////////////////////////////////////////////
/// @fn corecanariasjsPtr corecanariasjsAPI::getPlugin()
///
/// @brief  Gets a reference to the plugin that was passed in when the object
///         was created.  If the plugin has already been released then this
///         will throw a FB::script_error that will be translated into a
///         javascript exception in the page.
///////////////////////////////////////////////////////////////////////////////
corecanariasjsPtr corecanariasjsAPI::getPlugin()
{
    corecanariasjsPtr plugin(m_plugin.lock());
    if (!plugin) {
        throw FB::script_error("The plugin is invalid");
    }
    return plugin;
}

// Read/Write property testString
std::string corecanariasjsAPI::get_testString()
{
    return m_testString;
}

void corecanariasjsAPI::set_testString(const std::string& val)
{
    m_testString = val;
}

// Read-only property version
std::string corecanariasjsAPI::get_version()
{
    return FBSTRING_PLUGIN_VERSION;
}

void corecanariasjsAPI::testEvent()
{
    fire_test();
}

// extracted from NPAPI-chrome-file-api
bool corecanariasjsAPI::createDirectory(std::string strPath)
{
    if (!strPath.length())
    {
        return true;
    }
    boost::filesystem::path p(strPath);
    
    boost::filesystem::create_directory(p);
    return boost::filesystem::is_directory(p);
}

// extracted from NPAPI-chrome-file-api
FB::VariantList corecanariasjsAPI::getDirEntries(std::string strPath)
{
    boost::filesystem::path p(strPath);
    
    FB::VariantList entries;
    if (boost::filesystem::is_directory(p))
    {
        boost::filesystem::directory_iterator di(p);
        boost::filesystem::directory_iterator eos;
        
        while(di != eos)
        {
            boost::filesystem::directory_entry de = *di;
           // variant entry;
            //entry.assign<std::string>();
            entries.push_back(de.path().filename().generic_string());
            di++;
        }
    }
    return entries;
    
}