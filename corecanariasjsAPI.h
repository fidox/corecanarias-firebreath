/**********************************************************\

  Auto-generated corecanariasjsAPI.h

\**********************************************************/

#include <string>
#include <sstream>
#include <boost/weak_ptr.hpp>
#include "JSAPIAuto.h"
#include "BrowserHost.h"
#include "corecanariasjs.h"

#ifndef H_corecanariasjsAPI
#define H_corecanariasjsAPI

class corecanariasjsAPI : public FB::JSAPIAuto
{
public:
    ////////////////////////////////////////////////////////////////////////////
    /// @fn corecanariasjsAPI::corecanariasjsAPI(const corecanariasjsPtr& plugin, const FB::BrowserHostPtr host)
    ///
    /// @brief  Constructor for your JSAPI object.
    ///         You should register your methods, properties, and events
    ///         that should be accessible to Javascript from here.
    ///
    /// @see FB::JSAPIAuto::registerMethod
    /// @see FB::JSAPIAuto::registerProperty
    /// @see FB::JSAPIAuto::registerEvent
    ////////////////////////////////////////////////////////////////////////////
    corecanariasjsAPI(const corecanariasjsPtr& plugin, const FB::BrowserHostPtr& host) :
        m_plugin(plugin), m_host(host)
    {
        registerMethod("echo",      make_method(this, &corecanariasjsAPI::echo));
        registerMethod("testEvent", make_method(this, &corecanariasjsAPI::testEvent));
        registerMethod("createDirectory",      make_method(this, &corecanariasjsAPI::createDirectory));
        registerMethod("getDirEntries",      make_method(this, &corecanariasjsAPI::getDirEntries));
		
        // Read-write property
        registerProperty("testString",
                         make_property(this,
                                       &corecanariasjsAPI::get_testString,
                                       &corecanariasjsAPI::set_testString));
        
        // Read-only property
        registerProperty("version",
                         make_property(this,
                                       &corecanariasjsAPI::get_version));
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// @fn corecanariasjsAPI::~corecanariasjsAPI()
    ///
    /// @brief  Destructor.  Remember that this object will not be released until
    ///         the browser is done with it; this will almost definitely be after
    ///         the plugin is released.
    ///////////////////////////////////////////////////////////////////////////////
    virtual ~corecanariasjsAPI() {};

    corecanariasjsPtr getPlugin();

    // Read/Write property ${PROPERTY.ident}
    std::string get_testString();
    void set_testString(const std::string& val);

    // Read-only property ${PROPERTY.ident}
    std::string get_version();

    // Method echo
    FB::variant echo(const FB::variant& msg);
    
	bool createDirectory(std::string path);
	FB::VariantList getDirEntries(std::string strPath);

    // Event helpers
    FB_JSAPI_EVENT(test, 0, ());
    FB_JSAPI_EVENT(echo, 2, (const FB::variant&, const int));

    // Method test-event
    void testEvent();

private:
    corecanariasjsWeakPtr m_plugin;
    FB::BrowserHostPtr m_host;

    std::string m_testString;
};

#endif // H_corecanariasjsAPI

